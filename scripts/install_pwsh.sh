#!/bin/sh

. /etc/os-release
wget -q https://packages.microsoft.com/config/debian/$VERSION_ID/packages-microsoft-prod.deb

dpkg -i packages-microsoft-prod.deb
rm packages-microsoft-prod.deb

apt-get update && apt-get install -y powershell

pwsh -Command Write-Host -NoNewline -ForegroundColor Green 'Installed: '
pwsh -version
