import type {StorybookConfig} from "@storybook/angular";

const config: StorybookConfig = {
  stories: ["../src/**/*.mdx", "../src/**/*.stories.@(js|jsx|mjs|ts|tsx)"],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@chromatic-com/storybook",
    "@storybook/addon-interactions",
  ],
  framework: {
    name: "@storybook/angular",
    options: {},
  },
  docs: {
    autodocs: "tag",
  },
  staticDirs: [
    // requirement of ngx-extended-pdf-viewer
    "../node_modules/ngx-extended-pdf-viewer/",
    // make application assets available in storybook
    "../src/assets",
    // make storybook assets available in storybook
    "../src/stories/assets"
  ]
};
export default config;
