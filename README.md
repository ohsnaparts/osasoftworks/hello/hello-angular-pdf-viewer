# HelloAngularPdfViewer

A small progressive web app that handles PDF viewing capabilities

* [DEMO: PWA]
* [DEMO: Storybook]

![][screenshot]

## Features

- **Progressive Web App**
  - Upon first visit, the app will be cached locally for offline use: [CanIUse][CanIUse_ServiceWorkers]
- **Static Web App**
  - The app statically pre-rendered
- **Gitlab Pages**
  - The static website is deployed on Gitlab Pages
- **Storybook**
  - A storybook of components is hosted on the `/storybook` route
- **Buzzwords**
  - Angular
  - Jest
  - Storybook
  - SCSS
  - PWA
  - SSR
  - Gitlab Pages
  - Gitlab CI/CD

## Requirements

- **node** ~18.13.0
- **npm** ~8.19.3
- **PWA** capable web browser

## Running

```pwsh
# application
npm run start

# storybook
npm run storybook
```

## Testing

Unit tests are covered in jest

```pwsh
npm run test
```

[CanIUse_ServiceWorkers]: https://caniuse.com/?search=service%20worker
[screenshot]: /images/screenshot.jpg
[DEMO: Storybook]: https://hello-angular-pdf-viewer-ohsnaparts-osasoftworks-cf88ad04862d50.gitlab.io/storybook/index.html
[DEMO: PWA]: https://hello-angular-pdf-viewer-ohsnaparts-osasoftworks-cf88ad04862d50.gitlab.io/pdf
