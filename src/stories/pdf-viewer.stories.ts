import {PdfViewerComponent} from "../app/pdf";
import {Meta, moduleMetadata, StoryObj} from "@storybook/angular";
import {ButtonComponent} from "./button.component";
import {PdfModule} from "../app/pdf/pdf.module";
import {NgxExtendedPdfViewerModule} from "ngx-extended-pdf-viewer";
import {Input} from "@angular/core";

const meta: Meta<PdfViewerComponent> = {
  title: 'Example/PdfViewer',
  component: PdfViewerComponent,
  decorators: [
    moduleMetadata({
      imports: [NgxExtendedPdfViewerModule]
    })
  ],
  tags: ['autodocs'],
  argTypes: {
    pdfSrc: {
      control: 'text',
    },
    searchText: {
      control: 'text',
    },
    matchRegex: {
      control: 'boolean'
    },
    highlightAll: {
      control: 'boolean'
    },
    matchCase: {
      control: 'boolean'
    },
    wholeWords: {
      control: 'boolean'
    },
    matchDiacritics: {
      control: 'boolean'
    }
  },
  args: {}
}

export default meta;

type Story = StoryObj<PdfViewerComponent>;

export const Primary: Story = {
  args: {
    pdfSrc: 'lorem-ipsum-policy.odt.pdf',
    searchText: 'Dolor est lorem ipsum dolor sit amet. Feugiat pretium nibh',
    highlightAll: true,
    matchRegex: false
  }
}


const articleNumber = 'Article 4';
const articleTitle = 'How reliable is the lorem?'
const plain = (str: string) => str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
export const RegexSearch: Story = {
  args: {
    pdfSrc: 'lorem-ipsum-policy.odt.pdf',
    searchText: `${plain(articleNumber)}( )+.( )+${plain(articleTitle)}`,
    matchRegex: true,
    highlightAll: true
  }
}
