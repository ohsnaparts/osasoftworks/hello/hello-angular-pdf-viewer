import {Component} from '@angular/core';
import {RouterModule, RouterOutlet} from '@angular/router';
import {QuoteComponent} from "./quote/quote.component";
import {PdfModule} from "./pdf/pdf.module";
import {CommonModule} from "@angular/common";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    RouterOutlet,
    QuoteComponent,
    PdfModule
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'hello-angular-pdf-viewer';
}
