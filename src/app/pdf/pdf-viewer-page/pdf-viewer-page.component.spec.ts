import {ComponentFixture, TestBed} from '@angular/core/testing';

import {PdfViewerPageComponent} from './pdf-viewer-page.component';
import {NO_ERRORS_SCHEMA} from "@angular/core";

describe('PdfViewerPageComponent', () => {
  let component: PdfViewerPageComponent;
  let fixture: ComponentFixture<PdfViewerPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [],
      declarations: [PdfViewerPageComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();

    fixture = TestBed.createComponent(PdfViewerPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
