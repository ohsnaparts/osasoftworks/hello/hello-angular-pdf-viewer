import { Component } from '@angular/core';
import {FindResultMatchesCount} from "ngx-extended-pdf-viewer";

@Component({
  selector: 'app-pdf-viewer-page',
  templateUrl: './pdf-viewer-page.component.html',
  styleUrl: './pdf-viewer-page.component.scss'
})
export class PdfViewerPageComponent {
  public pdfSrc: string = 'assets/test-pdf.pdf';
  public searchText: string = 'Dolor est lorem ipsum dolor sit amet. Feugiat pretium nibh';
  public searchResult?: FindResultMatchesCount = undefined;

  updateSearchResult(result: FindResultMatchesCount) {
    this.searchResult = result;
  }
}
