import {Component, EventEmitter, inject, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';
import {
  FindOptions, FindResultMatchesCount,
  FindState,
  IPDFViewerApplication,
  NgxExtendedPdfViewerService,
  PageRenderedEvent,
  PagesLoadedEvent,
  PdfDownloadedEvent,
  PdfLoadedEvent,
  PdfLoadingStartsEvent
} from "ngx-extended-pdf-viewer";
import {BehaviorSubject, combineLatest, debounceTime, distinctUntilChanged, filter, map, Subscription} from "rxjs";
import {nameof} from "../../nameof";


interface CustomPdfFindOptions extends FindOptions {
  matchRegex: boolean;
}


@Component({
  selector: 'app-pdf-viewer',
  templateUrl: './pdf-viewer.component.html',
  styleUrl: './pdf-viewer.component.scss'
})
export class PdfViewerComponent implements OnChanges, OnDestroy {
  private readonly pdfViewerService: NgxExtendedPdfViewerService = inject(NgxExtendedPdfViewerService);

  @Input() pdfSrc: string = null!;
  @Input() searchText?: string = undefined;
  @Input() matchRegex = false;
  @Input() highlightAll = false;
  @Input() matchCase = false;
  @Input() wholeWords = false;
  @Input() matchDiacritics = false;
  @Output() searchResult = new EventEmitter<FindResultMatchesCount>();

  private readonly subscriptionsToDispose: Subscription = new Subscription();
  private readonly searchText$ = new BehaviorSubject<string | undefined>(undefined);
  private readonly allPagesLoaded$ = new BehaviorSubject<boolean>(false);
  private readonly searchTextAfterPagesLoaded$ = this.getSearchTextAfterPagesLoaded$();

  private readonly searchTextDebounceTimeMs = 500;
  private readonly defaultFindOptions: CustomPdfFindOptions = {
    matchCase: this.matchCase,
    highlightAll: this.highlightAll,
    wholeWords: this.wholeWords,
    matchDiacritics: this.matchDiacritics,
    matchRegex: this.matchRegex
  };

  private originalConvertToRegExpString: any;


  constructor() {
    this.subscribeToSearchTextChangesAfterPagesLoaded(
      (searchText: string) => this.dispatchFind(searchText)
    );
  }

  public ngOnChanges(changes: SimpleChanges): void {
    let searchTextChanges = changes['searchText'];
    if (searchTextChanges) {
      this.searchText$.next(searchTextChanges.currentValue);
    }

    this.defaultFindOptions.matchRegex = this.matchRegex;
    this.defaultFindOptions.matchDiacritics = this.matchDiacritics;
    this.defaultFindOptions.wholeWords = this.wholeWords;
    this.defaultFindOptions.highlightAll = this.highlightAll;
    this.defaultFindOptions.matchCase = this.matchCase;
    this.dispatchFind(this.searchText ?? '');
  }

  public ngOnDestroy(): void {
    this.subscriptionsToDispose.unsubscribe();
    this.restoreFindFeature();
  }

  public pageRendered($event: PageRenderedEvent) {
    console.log("Page rendered: %o", $event);
  }

  public pdfDownloaded($event: PdfDownloadedEvent) {
    console.log("PDF downloaded: %o", $event);
  }

  public pdfLoaded($event: PdfLoadedEvent) {
    console.log("PDF loaded: %o", $event);
    this.overrideFindFeature();
  }

  private overrideFindFeature(): void {
    console.info("Overriding PDF find feature to support regular expressions...");

    const findController = this.pdfViewerApplication.findController as any;

    // back up original so that we can revert or use it as fallback
    this.originalConvertToRegExpString = findController._convertToRegExpString;

    // override with our own implementation
    findController._convertToRegExpString = (query: string, ...args: any[]) => {
      const {matchRegex} = findController.state;
      return !matchRegex
        ? this.originalConvertToRegExpString.call(findController, query, ...args)
        : [false, query];
    }
  }

  private restoreFindFeature() {
    console.info("Restoring find feature to original state");
    if (this.originalConvertToRegExpString) {
      const findController = this.pdfViewerApplication.findController as any;
      findController._convertToRegExpString = this.originalConvertToRegExpString;
    }
  }

  public pdfLoadingFailed($event: Error) {
    console.error("PDF loading failed: %o", $event);
  }

  public pdfLoadingStarts($event: PdfLoadingStartsEvent) {
    console.log("PDF loading started: %o", $event);
  }

  public updateFindState($event: FindState) {
    console.log("PDF find state changed: %o", $event);
  }

  public allPagesLoaded($event: PagesLoadedEvent) {
    console.log("PDF all pages loaded: %o", $event);
    this.allPagesLoaded$.next(true);
  }

  private get pdfViewerApplication(): IPDFViewerApplication {
    if (!window.PDFViewerApplication) {
      throw new Error(
        `Unable to find required global property ${nameof<Window>('PDFViewerApplication')}.
         This property is implemented by the ngx-extended-pdf-viewer component.
         Are all dependencies loaded?`
      );
    }
    return window.PDFViewerApplication;
  }

  private findTextInPdf(text: string) {
    console.log("Searching for text: %s", text);
    this.pdfViewerService.find(text, {
      matchCase: false,
      highlightAll: true,
      wholeWords: false,
      matchDiacritics: false
    });
  }

  private subscribeToSearchTextChangesAfterPagesLoaded(
    searchTextChangedFn: (searchText: string) => void
  ) {
    this.subscriptionsToDispose.add(
      this.searchTextAfterPagesLoaded$.subscribe(
        (searchText: string) => searchTextChangedFn(
          searchText ?? ''
        )
      )
    );
  }

  private getSearchTextAfterPagesLoaded$() {
    // this is to ensure that searches are
    // 1. applied only after the PDF has been fully loaded / text is available to search
    // 2. to store search requests while the PDF is loaded
    return combineLatest([
      this.searchText$,
      this.allPagesLoaded$
    ]).pipe(
      distinctUntilChanged(),
      debounceTime(this.searchTextDebounceTimeMs),
      filter(([_, allPagesLoaded]) => !!allPagesLoaded),
      map(([searchText, _]) => searchText ?? '')
    );
  }

  private dispatchFind(type: string, findPrevious = false): void {
    this.pdfViewerApplication.eventBus.dispatch('find', {
      ...this.defaultFindOptions,
      query: this.searchText,
      type,
      findPrevious,
      source: undefined
    });
  }

  public updateFindMatchesCount(matches: FindResultMatchesCount) {
    console.log("updateFindMatchesCount: Found %o", matches);
    this.searchResult.emit(matches);
  }
}
