import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {provideRouter} from "@angular/router";
import {
  PdfViewerComponent,
  PdfViewerPageComponent
} from ".";
import {NgxExtendedPdfViewerModule} from "ngx-extended-pdf-viewer";

@NgModule({
  declarations: [
    PdfViewerPageComponent,
    PdfViewerComponent
  ],
    imports: [
        CommonModule,
        NgxExtendedPdfViewerModule
    ],
  exports: [],
  providers: []
})
export class PdfModule {
}
