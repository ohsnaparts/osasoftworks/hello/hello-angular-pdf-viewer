import {Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {PdfViewerPageComponent} from "./pdf-viewer-page/pdf-viewer-page.component";

export const PDF_ROUTES: Routes = [
  {path: 'pdf', component: PdfViewerPageComponent}
];
