import {Routes} from '@angular/router';
import {PDF_ROUTES} from "./pdf/pdf.routes";

export const APP_ROUTES: Routes = [
  ...PDF_ROUTES
];
