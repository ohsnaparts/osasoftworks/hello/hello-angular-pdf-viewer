import { nameof } from './nameof';

describe('nameof', () => {
  it('returns the validated variable name', () => {
    const actualName = nameof<Math>('PI');
    expect(actualName).toBe('PI');
  });

  it('returns the validated function name', () => {
    const actualName = nameof<Math>('sqrt');
    expect(actualName).toBe('sqrt');
  });
});
