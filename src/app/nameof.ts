/**
 * Provides a compile-time save way to reference an object member by name
 * @param memberName
 * @example nameof<Math>('PI'); => 'PI'
 * @example nameof<Math>('NotPI'); => linter error
 * @example Math[nameof<Math>('PI')] => 3.141592653589793
 */
export function nameof<T>(memberName: keyof T): string {
  return memberName as string;
}
