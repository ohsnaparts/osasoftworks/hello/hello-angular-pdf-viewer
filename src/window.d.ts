import {IPDFViewerApplication} from "ngx-extended-pdf-viewer";

export declare global {
  interface Window {
    /**
     * @summary added by ngx-extended-pdf-viewer
     * @see https://github.com/stephanrauh/ngx-extended-pdf-viewer/blob/d0d395dd77bb74f5fca58ff7489ef968a26d2754/projects/ngx-extended-pdf-viewer/src/lib/options/pdf-viewer-application.ts#L1053
     * @see https://github.com/stephanrauh/ngx-extended-pdf-viewer/issues/2339
     * @see https://github.com/stephanrauh/extended-pdf-viewer-showcase/commit/c5938c2bd4cd0e423f557cb5d79d859b2e031753?diff=unified&w=0#diff-57ae492cee24b49031d0ea960d5c54b59ba94a665fc6213fc921554f017ad528
     */
    PDFViewerApplication?: IPDFViewerApplication;
  }
}
